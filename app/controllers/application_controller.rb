class ApplicationController < ActionController::Base

  def mensajeAlertas

    render :partial => "mensajeAlertas"
  end

  #protect_from_forgery with: :exception
  #protect_from_forgery prepend: true
  #before_action :configure_permitted_parameters, if: :devise_controller?

  protect_from_forgery with: :exception
  protect_from_forgery prepend: true
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :set_locale
  before_action :release_unless_alerts

  # before_action :set_locale

  private
  def set_locale
    I18n.locale = params[:locale] #|| I18n.default_locale
    #Rails.application.routes.default_url_options[:locale]= I18n.locale
  end

  def self.default_url_options(options={})
    options.merge({:locale => I18n.locale})
  end

  def release_unless_alerts
    if current_user
      @metodo = action_name
      @user_id = current_user.id
      if (action_name != "creacion_observaciones") && (action_name != "observaciones") && (action_name != "alertTran") && (action_name != "alertPer") && (action_name != "par4") && (action_name != "par3") && (action_name != "par2") && (action_name != "alertaBoton") && (action_name != "alertaPerfiles") && (action_name != "alertaTransaccional") && (action_name != "contadorSuperior") && (action_name != "mensajeAlerta") && (action_name != "pie1") && (action_name != "pie2") && (action_name != "pie3") && (action_name != "totalAlerts") && (action_name != "totalAtendidas") && (action_name != "totalPoratender")
        @liberacion_alertas = Tbitacora.where(:IdEstado => 1).where(:IdTipoReg => 3).where(:canceladas => 0).where(:IdUsuario => current_user.id)

        if @liberacion_alertas.present? && !@liberacion_alertas.nil?
          @liberacion_alertas.each do |lib|
            lib.IdUsuario = "N/A"
            lib.save
          end
        end

        @atendiendo = Tatendiendo.where(:IdUsuario => current_user.id)

        if @atendiendo.present? && !@atendiendo.nil?
          @atendiendo.each do |atn|
            atn.destroy
          end
        end
      end

      @atn = Tatendidasxhoy.where(:fecha => Time.now.strftime("%Y-%m-%d")).where(:idProducto => 1).count
      if @atn == 0
        atn = Tatendidasxhoy.new
        atn.fecha = Time.now.strftime("%Y-%m-%d")
        atn.idProducto = 1
        atn.cantidad = 0
        atn.save
      end


      @atn = Tatendidasxhoy.where(:fecha => Time.now.strftime("%Y-%m-%d")).where(:idProducto => 2).count
      if @atn == 0
        atn = Tatendidasxhoy.new
        atn.fecha = Time.now.strftime("%Y-%m-%d")
        atn.idProducto = 2
        atn.cantidad = 0
        atn.save
      end
    end
  end
  protected

  # private
  # def set_locale
  #   locale = params[:locale]
  #   I18n.locale = params[:locale]
  #   #|| I18n.default_locale
  #   #Rails.application.routes.default_url_options[:locale]= I18n.locale
  # end

  # def set_locale
  #   locale = params[:locale].to_s.strip.to_sym
  #   I18n.locale = I18n.available_locales.include?(locale) ?
  #       locale :
  #       I18n.default_locale
  # end

  # def self.default_url_options(options={})
  #   idioma = I18n.locale
  #   options.merge({ :locale => I18n.locale })
  # end

  # def default_url_options
  #   { locale: I18n.locale }
  # end

  def configure_permitted_parameters
    @rails = Gem::Specification.find_by_name('rails')
    @railsn = @rails.version

    @version = @railsn.to_s.split('.')[0]

    if @version == "5"
      puts '5'
      # devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :password, :password_confirmation, :profile_id, :area_id) }
      # devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit(:login, :user_name, :email, :password, :remember_me, :birthday, :profile_id) }
      # devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :current_password, :profile_id, :area_id) }

      devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :password, :password_confirmation, :area_id])
      devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :user_name, :email, :password, :remember_me, :birthday, :profile_id])
      devise_parameter_sanitizer.permit(:account_update, keys: [:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :current_password, :profile_id, :area_id])

      # devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name, :name, :email, :password, :password_confirmation, :last_name, :birthday, :boss_name, :phone, :ext, :name_profile_id, :area, :employment])
      # devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :user_name, :email, :password, :remember_me, :birthday, :name_profile_id])
      # devise_parameter_sanitizer.permit(:account_update, keys: [:username, :name, :email, :last_name, :birthday, :boss_name, :phone, :ext, :area, :employment, :profile_id, :current_password, :name_profile_id])


    elsif @version == "4"
      puts '4'
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :password, :password_confirmation, :profile_id, :area_id) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :user_name, :email, :password, :remember_me, :birthday, :profile_id) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :current_password, :profile_id, :area_id) }
    end
  end
end
