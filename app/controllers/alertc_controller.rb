class AlertcController < ApplicationController
  @@byConTran = "Consult Alert Transaccional"
  @@byConPer = "Consult Alert Perfiles"

  #@@byConTran = "Consult Alert Transaccional"
  #@@byConPer = "Consult Alert Perfiles"


  def index
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per_c_tran = Permission.where("view_name = 'Consult Alert Transaccional' and profile_id = ?", @cu)

        @per_c_tran.each do |permisos|
          @crearConTran = permisos.crear
          @leerConTran = permisos.leer

          if permisos.view_name == @@byConTran

            @@crearConTran = permisos.crear
            @@editarConTran = permisos.editar
            @@leerConTran = permisos.leer
            @@eliminarConTran = permisos.eliminar

            @crearConTran = permisos.crear
            @editarConTran = permisos.editar
            @leerConTran = permisos.leer
            @eliminarConTran = permisos.eliminar
          end
        end

        @per_c_per = Permission.where("view_name = 'Consult Alert Perfiles' and profile_id = ?", @cu)

        @per_c_per.each do |permisos|
          @crearConPer = permisos.crear
          @leerConPer = permisos.leer

          if permisos.view_name == @@byConPer

            @@crearConPer = permisos.crear
            @@editarConPer = permisos.editar
            @@leerConPer = permisos.leer
            @@eliminarConPer = permisos.eliminar

            @crearConPer = permisos.crear
            @editarConPer = permisos.editar
            @leerConPer = permisos.leer
            @eliminarConPer = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if (@@leerConTran == 2 || @@leerConPer == 2)
# Variables para campos del formulario
            @campos = Tcamposlay.where.not(:Alias => "")
            @estados = Testadosxalerta.all
            @usuarios = User.all
            @grupos = Tgrupos.all
            @filtros = TxFilters.all

            per = Tperfiles.all
            gon.perfiles = per
            gon.false = true

            # fin variables para campos del formulario
            #
            # Variables para index

            # if @@leerConTran == 2 && @@leerConPer == 2
            #   @alertas = Tbitacora.where(:IdTipoReg => 3).page(params[:page]).per(20)
            # elsif @@leerConTran == 2 && @@leerConPer != 2
            #   @alertas = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).page(params[:page]).per(20)
            # elsif @@leerConTran != 2 && @@leerConPer == 2
            #   @alertas = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).page(params[:page]).per(20)
            # end

            #------------------------------------------------------------------------- Búsquedas

            if params[:busqueda].present?
              @producto = params[:product]
              query = ""
              @query = ""

              if @producto == "1" && @@leerConTran == 2 #------------------------------ Búsquedas Transaccional
                @ticket = params[:ticket]
                @message = params[:message]
                @valor_ci = params[:ValorCI_t]
                @layout = params[:CAMPOSLAY]
                @valor_layout = params[:ValorLAY]
                if @valor_layout.nil?
                  @valor_layout = ""
                end
                @estado = params[:ESTADOSALERTA]
                @user = params[:USUARIOS]
                @start_date = params[:FechaIni]
                @end_date = params[:FechaFin].to_s
                @tipo = params[:TIPOALERTA]
                @filters = params[:FILTROS]

                if @ticket != ""
                  query = query + "Id = " + @ticket.to_s
                  @query = @query + "Ticket = " + @ticket.to_s
                end

                if @message != ""
                  if @ticket != ""
                    query = query + " and Mensaje like '%" + @message.to_s + "%'"
                    @query = @query + ", " + t('views.alertc_index.message') + " = " + @message.to_s
                  else
                    query = query + "Mensaje like '%" + @message.to_s + "%'"
                    @query = @query + t('views.alertc_index.message') + " = " + @message.to_s
                  end
                end

                if @valor_ci != ""
                  if @ticket != "" || @message != ""
                    query = query + " and IdTran like '%" + @valor_ci.to_s + "%'"
                    @query = @query + ", " + t('views.alertc_index.placeh_value_ci') + " = " + @valor_ci.to_s
                  else
                    query = query + "IdTran like '%" + @valor_ci.to_s + "%'"
                    @query = @query + t('views.alertc_index.placeh_value_ci') + " = " + @valor_ci.to_s
                  end
                end

                if @layout != ""
                  if @valor_layout != ""
                    if @ticket != "" || @message != "" || @valor_ci != ""
                        query = query + " and Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                        @query = @query + ", " + t('views.alertc_index.placeh_layout') + ": " + @layout + " = " + @valor_layout.to_s
                    else
                      query = query + "Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                      @query = @query + t('views.alertc_index.placeh_layout') + ": " + @layout + " = " + @valor_layout.to_s
                    end
                  else
                    @error = true
                    @alertas = @alertas.page(params[:page]).per(20)
                  end
                end

                if @estado != ""
                  if @ticket != "" || @message != "" || @valor_ci != "" || @layout != ""
                    @estado_find = Testadosxalerta.where(:IdEstado => @estado).first
                    query = query + " and IdEstado = " + @estado.to_s
                    @query = @query + ", " + t('views.alertc_index.state') + " = " + @estado_find.Descripcion
                  else
                    @estado_find = Testadosxalerta.where(:IdEstado => @estado).first
                    query = query + "IdEstado = " + @estado.to_s
                    @query = @query + t('views.alertc_index.state') + " = " + @estado_find.Descripcion
                  end
                end

                if @user != ""
                  if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != ""
                    query = query + " and IdUsuario = " + @user.to_s
                    @user_find = User.find(@user.to_i)
                    @query = @query + ", " + t('views.alertc_index.placeh_user') + " = " + @user_find.name.to_s + " " + @user_find.last_name.to_s
                  else
                    # query = query + "IdUsuario = " + @user.to_s
                    query = query + "IdUsuario = '" + @user.to_s + "'"
                    @user_find = User.find(@user.to_i)
                    @query = @query + t('views.alertc_index.placeh_user') + " = " + @user_find.name.to_s + " " + @user_find.last_name.to_s
                  end
                end

                if @start_date != ""
                  @start_date = @start_date.to_date.strftime('%Y-%m-%d')
                  if @end_date != ""
                    @end_date = @end_date.to_date.strftime('%Y-%m-%d')
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != ""
                      query = query + " and Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      @query = @query + ", " + t('views.alertc_index.start_date') + " " + t('views.alertc_index.between') + " " + @start_date.to_s + " " + t('views.alertc_index.and') + " " + @end_date.to_s
                    else
                      query = query + "Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      @query = @query + t('views.alertc_index.start_date') + " " + t('views.alertc_index.between') + " " + @start_date.to_s + " " + t('views.alertc_index.and') + " " + @end_date.to_s
                    end
                  end
                end

                if @filters != ""
                  if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != ""
                    query = query + " and IdFiltro = " + @filters
                    @filter_find = TxFilters.find(@filters.to_i)
                    @query = @query + ", " + t('views.alertc_index.filter') + " = " + @filter_find.Description.to_s
                  else
                    query = query + "IdFiltro = " + @filters
                    @filter_find = TxFilters.find(@filters.to_i)
                    @query = @query + t('views.alertc_index.filter') + " = " + @filter_find.Description.to_s
                  end
                end

                if @tipo != ""
                  if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != "" || @filters != ""
                    query = query + " and canceladas = " + @tipo
                    if @tipo == "0"
                      @tipo_find = t('views.alertc_index.nomal')
                    else
                      @tipo_find = t('views.alertc_index.canceled')
                    end
                    @query = @query + ", " + t('views.alertc_index.placeh_type') + " = " + @tipo_find
                  else
                    query = query + "canceladas = " + @tipo
                    if @tipo == "0"
                      @tipo_find = t('views.alertc_index.nomal')
                    else
                      @tipo_find = t('views.alertc_index.canceled')
                    end
                    @query = @query + t('views.alertc_index.placeh_type') + " = " + @tipo_find
                  end
                end

                @alertas = Tbitacora.where(query + " and IdProducto = 1 and IdTipoReg = 3").page(params[:page]).per(20)

                if !@alertas.present? || @alertas.count() == 0
                  @error = true
                  @alertas = @alertas.page(params[:page]).per(20)
                end
              else
                if @@leerConPer == 2

                  @ticket = params[:ticket]
                  @message = params[:message]
                  @valor_ci = params[:ValorCI_t]
                  @layout = params[:CAMPOSLAY]
                  @valor_layout = params[:ValorLAY]
                  @estado = params[:ESTADOSALERTA]
                  @user = params[:USUARIOS]
                  @start_date = params[:FechaIni]
                  @end_date = params[:FechaFin].to_s
                  @tipo = params[:TIPOALERTA]
                  @grupos_s = params[:GRUPOS]
                  @perfil = params[:PERFILES]

                  if @ticket != ""
                    query = query + "Id = " + @ticket.to_s
                    @query = @query + "Ticket = " + @ticket.to_s
                  end

                  if @message != ""
                    if @ticket != ""
                      query = query + " and Mensaje like '%" + @message.to_s + "%'"
                      @query = @query + ", " + t('views.alertc_index.message') + " = " + @message.to_s
                    else
                      query = query + "Mensaje like '%" + @message.to_s + "%'"
                      @query = @query + t('views.alertc_index.message') + " = " + @message.to_s
                    end
                  end

                  if @valor_ci != ""
                    if @ticket != "" || @message != ""
                      query = query + " and IdTran like '%" + @valor_ci.to_s + "%'"
                      @query = @query + ", " + t('views.alertc_index.placeh_value_ci') + " = " + @valor_ci.to_s
                    else
                      query = query + "IdTran like '%" + @valor_ci.to_s + "%'"
                      @query = @query + t('views.alertc_index.placeh_value_ci') + " = " + @valor_ci.to_s
                    end
                  end

                  if @layout != ""
                    if @valor_layout != ""
                      if @ticket != "" || @message != "" || @valor_ci != ""
                        query = query + " and Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                        @query = @query + ", " + t('views.alertc_index.placeh_layout') + ": " + @layout + " = " + @valor_layout.to_s
                      else
                        query = query + "Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                        @query = @query + t('views.alertc_index.placeh_layout') + ": " + @layout + " = " + @valor_layout.to_s
                      end
                    else
                      @error = true
                      @alertas = @alertas.page(params[:page]).per(20)
                    end
                  end

                  if @estado != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != ""
                      @estado_find = Testadosxalerta.where(:IdEstado => @estado).first
                      query = query + " and IdEstado = " + @estado.to_s
                      @query = @query + ", " + t('views.alertc_index.state') + " = " + @estado_find.Descripcion
                    else
                      @estado_find = Testadosxalerta.where(:IdEstado => @estado).first
                      query = query + "IdEstado = " + @estado.to_s
                      @query = @query + t('views.alertc_index.state') + " = " + @estado_find.Descripcion
                    end
                  end

                  if @user != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != ""
                      query = query + " and IdUsuario = " + @user.to_s
                      @user_find = User.find(@user.to_i)
                      @query = @query + ", " + t('views.alertc_index.placeh_user') + " = " + @user_find.name.to_s + " " + @user_find.last_name.to_s
                    else
                      # query = query + "IdUsuario = " + @user.to_s
                      query = query + "IdUsuario = '" + @user.to_s + "'"
                      @user_find = User.find(@user.to_i)
                      @query = @query + t('views.alertc_index.placeh_user') + " = " + @user_find.name.to_s + " " + @user_find.last_name.to_s
                    end
                  end

                  if @start_date != ""
                    @start_date = @start_date.to_date.strftime('%Y-%m-%d')
                    if @end_date != ""
                      @end_date = @end_date.to_date.strftime('%Y-%m-%d')
                      if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != ""
                        query = query + " and Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                        @query = @query + ", " + t('views.alertc_index.start_date') + " " + t('views.alertc_index.between') + " " + @start_date.to_s + " " + t('views.alertc_index.and') + " " + @end_date.to_s
                      else
                        query = query + "Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                        @query = @query + t('views.alertc_index.start_date') + " " + t('views.alertc_index.between') + " " + @start_date.to_s + " " + t('views.alertc_index.and') + " " + @end_date.to_s
                      end
                    end
                  end

                  if @grupos_s != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != ""
                      query = query + " and IdGrupo = " + @grupos_s
                      @grupo_find = Tgrupos.where(:IdGrupo => @grupos_s.to_i).first
                      @query = @query + ", " + t('views.alertc_index.group') + " = " + @grupo_find.Nombre.to_s
                    else
                      query = query + "IdGrupo = " + @grupos_s
                      @grupo_find = Tgrupos.where(:IdGrupo => @grupos_s.to_i).first
                      @query = @query + t('views.alertc_index.group') + " = " + @grupo_find.Nombre.to_s
                    end
                  end

                  if @perfil != "" && !@perfil.nil?
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != "" || @grupos_s != ""
                      query = query + " and IdPerfil = " + @perfil
                      @perfil_find = Tperfiles.where(:IdPerfil => @perfil.to_i).first
                      @query = @query + ", " + t('views.alertc_index.profile') + " = " + @perfil_find.Nombre.to_s
                    else
                      query = query + "IdGrupo = " + @filters
                      @perfil_find = Tperfiles.where(:IdPerfil => @perfil.to_i).first
                      @query = @query + t('views.alertc_index.profile') + " = " + @perfil_find.Nombre.to_s
                    end
                  end

                  if @tipo != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != "" || @filters != ""
                      query = query + " and canceladas = " + @tipo
                      if @tipo == "0"
                        @tipo_find = t('views.alertc_index.nomal')
                      else
                        @tipo_find = t('views.alertc_index.canceled')
                      end
                      @query = @query + ", " + t('views.alertc_index.placeh_type') + " = " + @tipo_find
                    else
                      query = query + "canceladas = " + @tipo
                      if @tipo == "0"
                        @tipo_find = t('views.alertc_index.nomal')
                      else
                        @tipo_find = t('views.alertc_index.canceled')
                      end
                      @query = @query + t('views.alertc_index.placeh_type') + " = " + @tipo_find
                    end
                  end

                  @alertas = Tbitacora.where(query + " and IdProducto = 2 and IdTipoReg = 3").page(params[:page]).per(20)

                  if !@alertas.present? || @alertas.count() == 0
                    @error = true
                    @alertas = @alertas.page(params[:page]).per(20)
                  end
                end
              end
              #---------------------------------------------------------------------------- FIN Búsquedas
            else
              if @@leerConTran == 2 && @@leerConPer == 2
                @hoyDate = Time.now.strftime("%Y-%m-%d").split('-')
                @año = @hoyDate[0]
                @mes = @hoyDate[1]

                @alertas = ActiveRecord::Base.connection.exec_query("
                select tb.Id, tb.Fecha, tb.Hora, tb.IdTran, tb.IdProducto, tb.Mensaje, tb.Referencia, cast(te.IdEstado as nvarchar) + '-'+ te.Descripcion IdEstado, fi.Description Filtro, tg.Nombre Grupo, cast(tp.IdPerfil as nvarchar) + '-'+ tp.Nombre Perfil ,tb.IdUsuario, contadores.cantidad
                from tbitacora tb
                join (select min(tbt.Id) idAlerta, tbt.IdFiltro, COUNT(*) cantidad, IdTran, tbt.Fecha
                from tbitacora tbt
                where IdTipoReg = 3 and IdEstado != 2
                and MONTH(tbt.Fecha) = #{@mes}
                and YEAR(tbt.Fecha) = #{@año}
                and tbt.IdFiltro not in (select idFiltro from filtersnos)
                group by IdTran, tbt.Fecha, tbt.IdFiltro) as contadores  on tb.Id = contadores.idAlerta
                left join testadosxalerta te on te.IdEstado = tb.IdEstado
                left join tx_filters fi on fi.Session_id = tb.IdFiltro
                left join tgrupos tg on tg.IdGrupo = tb.IdGrupo
                left join tperfiles tp on tp.IdGrupo = tb.IdGrupo and tp.IdPerfil = tb.IdPerfil
                order by tb.Fecha DESC
                ")

                alertasTabla = Array.new
                @alertas.each do | dat |
                  alertasTabla.push(dat)
                end

                @alertasTabla = Kaminari.paginate_array(alertasTabla).page(params[:page]).per(20)

              elsif @@leerConTran == 2 && @@leerConPer != 2

                @hoyDate = Time.now.strftime("%Y-%m-%d").split('-')
                @año = @hoyDate[0]
                @mes = @hoyDate[1]
                # @alertas = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).page(params[:page]).per(20)


                @alertas = ActiveRecord::Base.connection.exec_query("
                select tb.Id, tb.Fecha, tb.Hora, tb.IdTran, tb.IdProducto, tb.Mensaje, tb.Referencia, cast(te.IdEstado as nvarchar) + '-'+ te.Descripcion IdEstado, fi.Description Filtro, tg.Nombre Grupo, cast(tp.IdPerfil as nvarchar) + '-'+ tp.Nombre Perfil ,tb.IdUsuario, contadores.cantidad
                from tbitacora tb
                join (select min(tbt.Id) idAlerta, tbt.IdFiltro, COUNT(*) cantidad, IdTran, tbt.Fecha
                from tbitacora tbt
                where IdTipoReg = 3 and IdEstado != 2 and IdProducto = 1
                and MONTH(tbt.Fecha) = #{@mes}
                and YEAR(tbt.Fecha) = #{@año}
                and tbt.IdFiltro not in (select idFiltro from filtersnos)
                group by IdTran, tbt.Fecha, tbt.IdFiltro) as contadores  on tb.Id = contadores.idAlerta
                left join testadosxalerta te on te.IdEstado = tb.IdEstado
                left join tx_filters fi on fi.Session_id = tb.IdFiltro
                left join tgrupos tg on tg.IdGrupo = tb.IdGrupo
                left join tperfiles tp on tp.IdGrupo = tb.IdGrupo and tp.IdPerfil = tb.IdPerfil
                order by tb.Fecha DESC
                ")

                alertasTabla = Array.new
                @alertas.each do | dat |
                  alertasTabla.push(dat)
                end

                @alertasTabla = Kaminari.paginate_array(alertasTabla).page(params[:page]).per(20)



              elsif @@leerConTran != 2 && @@leerConPer == 2
                # @alertas = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).page(params[:page]).per(20)
                @hoyDate = Time.now.strftime("%Y-%m-%d").split('-')
                @año = @hoyDate[0]
                @mes = @hoyDate[1]

                @alertas = ActiveRecord::Base.connection.exec_query("
                select tb.Id, tb.Fecha, tb.Hora, tb.IdTran, tb.IdProducto, tb.Mensaje, tb.Referencia, cast(te.IdEstado as nvarchar) + '-'+ te.Descripcion IdEstado, fi.Description Filtro, tg.Nombre Grupo, cast(tp.IdPerfil as nvarchar) + '-'+ tp.Nombre Perfil ,tb.IdUsuario, contadores.cantidad
                from tbitacora tb
                join (select min(tbt.Id) idAlerta, tbt.IdFiltro, COUNT(*) cantidad, IdTran, tbt.Fecha
                from tbitacora tbt
                where IdTipoReg = 3 and IdEstado != 2 and IdProducto = 2
                and MONTH(tbt.Fecha) = #{@mes}
                and YEAR(tbt.Fecha) = #{@año}
                and tbt.IdFiltro not in (select idFiltro from filtersnos)
                group by IdTran, tbt.Fecha, tbt.IdFiltro) as contadores  on tb.Id = contadores.idAlerta
                left join testadosxalerta te on te.IdEstado = tb.IdEstado
                left join tx_filters fi on fi.Session_id = tb.IdFiltro
                left join tgrupos tg on tg.IdGrupo = tb.IdGrupo
                left join tperfiles tp on tp.IdGrupo = tb.IdGrupo and tp.IdPerfil = tb.IdPerfil
                order by tb.Fecha DESC
                ")

                alertasTabla = Array.new
                @alertas.each do | dat |
                  alertasTabla.push(dat)
                end

                @alertasTabla = Kaminari.paginate_array(alertasTabla).page(params[:page]).per(20)
              end

            end

            #------------------------------------------------------------------------- XLS
            @xls = params[:xls_g]
            if params[:xls_g].present?
              Thread.new do
                puts red('Comienza hilo-- CONSULTA DE ALERTAS')
                @user_id = params[:user_id]
                @idioma = params[:idioma]
                @producto = params[:product]
                query = ""

                if @producto == "1" #------------------------------ Búsquedas Transaccional
                  @ticket = params[:ticket]
                  @message = params[:message]
                  @valor_ci = params[:ValorCI_t]
                  @layout = params[:CAMPOSLAY]
                  @valor_layout = params[:ValorLAY]
                  if @valor_layout.nil?
                    @valor_layout = ""
                  end
                  @estado = params[:ESTADOSALERTA]
                  @user = params[:USUARIOS]
                  @start_date = params[:FechaIni]
                  @end_date = params[:FechaFin].to_s
                  @tipo = params[:TIPOALERTA]
                  @filters = params[:FILTROS]

                  if @ticket != ""
                    query = query + "Id = " + @ticket.to_s
                  end

                  if @message != ""
                    if @ticket != ""
                      query = query + " and Mensaje like '%" + @message.to_s + "%'"
                    else
                      query = query + "Mensaje like '%" + @message.to_s + "%'"
                    end
                  end

                  if @valor_ci != ""
                    if @ticket != "" || @message != ""
                      query = query + " and IdTran like '%" + @valor_ci.to_s + "%'"
                    else
                      query = query + "IdTran like '%" + @valor_ci.to_s + "%'"
                    end
                  end

                  if @layout != ""
                    if @valor_layout != ""
                      if @ticket != "" || @message != "" || @valor_ci != ""
                        query = query + " and Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                      else
                        query = query + "Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                      end
                    end
                  end

                  if @estado != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != ""
                      query = query + " and IdEstado = " + @estado.to_s
                    else
                      query = query + "IdEstado = " + @estado.to_s
                    end
                  end

                  if @user != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != ""
                      query = query + " and IdUsuario = " + @user.to_s
                    else
                      # Cambio de consulta
                      # query = query + "IdUsuario = " + @user.to_s
                      query = query + "IdUsuario = '" + @user.to_s + "'"
                    end
                  end

                  if @start_date != ""
                    @start_date = @start_date.to_date.strftime('%Y-%m-%d')
                    if @end_date != ""
                      @end_date = @end_date.to_date.strftime('%Y-%m-%d')
                      if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != ""
                        query = query + " and Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      else
                        query = query + "Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      end
                    end
                  end

                  if @filters != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != ""
                      query = query + " and IdFiltro = " + @filters
                    else
                      query = query + "IdFiltro = " + @filters
                    end
                  end

                  if @tipo != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != "" || @filters != ""
                      query = query + " and canceladas = " + @tipo
                    else
                      query = query + "canceladas = " + @tipo
                    end
                  end


                  @usuarios_all = User.all
                  @usuario_log = User.find(current_user.id)


                  puts cyan('Comienza generación de XLS-- TRANSACCIONAL')
                  @filtro_generador = 'general transaccional'
                  create_xls = CreateXls.new
                  @query = query
                  create_xls.setInfoAlertLevelXls( @filtro_generador, @usuario_log, @usuarios_all,@query, @idioma)
                  create_xls.alertLevelXLS
                else
                  @ticket = params[:ticket]
                  @message = params[:message]
                  @valor_ci = params[:ValorCI_t]
                  @layout = params[:CAMPOSLAY]
                  @valor_layout = params[:ValorLAY]
                  @estado = params[:ESTADOSALERTA]
                  @user = params[:USUARIOS]
                  @start_date = params[:FechaIni]
                  @end_date = params[:FechaFin].to_s
                  @tipo = params[:TIPOALERTA]
                  @grupos_s = params[:GRUPOS]
                  @perfil = params[:PERFILES]

                  if @ticket != ""
                    query = query + "Id = " + @ticket.to_s
                  end

                  if @message != ""
                    if @ticket != ""
                      query = query + " and Mensaje like '%" + @message.to_s + "%'"
                    else
                      query = query + "Mensaje like '%" + @message.to_s + "%'"
                    end
                  end

                  if @valor_ci != ""
                    if @ticket != "" || @message != ""
                      query = query + " and IdTran like '%" + @valor_ci.to_s + "%'"
                    else
                      query = query + "IdTran like '%" + @valor_ci.to_s + "%'"
                    end
                  end

                  if @layout != ""
                    if @valor_layout != ""
                      if @ticket != "" || @message != "" || @valor_ci != ""
                        query = query + " and Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                      else
                        query = query + "Referencia like '%" + @layout + "=" + @valor_layout.to_s + "%'"
                      end
                    end
                  end

                  if @estado != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != ""
                      query = query + " and IdEstado = " + @estado.to_s
                    else
                      query = query + "IdEstado = " + @estado.to_s
                    end
                  end

                  if @user != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != ""
                      query = query + " and IdUsuario = " + @user.to_s
                    else
                      # query = query + "IdUsuario = " + @user.to_s
                      query = query + "IdUsuario = '" + @user.to_s + "'"
                    end
                  end

                  if @start_date != ""
                    @start_date = @start_date.to_date.strftime('%Y-%m-%d')
                    if @end_date != ""
                      @end_date = @end_date.to_date.strftime('%Y-%m-%d')
                      if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != ""
                        query = query + " and Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      else
                        query = query + "Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      end
                    end
                  end

                  if @grupos_s != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != ""
                      query = query + " and IdGrupo = " + @grupos_s
                    else
                      query = query + "IdGrupo = " + @grupos_s
                    end
                  end

                  if @perfil != "" && !@perfil.nil?
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != "" || @grupos_s != ""
                      query = query + " and IdPerfil = " + @perfil
                    else
                      query = query + "IdGrupo = " + @filters
                    end
                  end

                  if @tipo != ""
                    if @ticket != "" || @message != "" || @valor_ci != "" || @layout != "" || @estado != "" || @user != "" || @start_date != "" || @filters != ""
                      query = query + " and canceladas = " + @tipo
                    else
                      query = query + "canceladas = " + @tipo
                    end
                  end

                  @usuarios_all = User.all
                  @usuario_log = User.find(current_user.id)


                  puts cyan('Comienza generación de XLS-- PERFILES')
                  @filtro_generador = 'general perfiles'
                  create_xls = CreateXls.new
                  @query = query
                  create_xls.setInfoAlertLevelXls(@filtro_generador, @usuario_log, @usuarios_all,@query, @idioma)
                  create_xls.alertLevelXLS
                end

              end
            end

            @atendidas = Hash.new
            @atend = Tatendiendo.all
            @atend.each do |atn|
              @atendidas.store(atn.IdTran,"")
            end

            puts @atendidas
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  def par1


    if current_user.habilitado == 0

      render :partial => "par1"
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  def par2
    if current_user.habilitado == 0
      @hora_ant = Time.now - 30
      @fecha = @hora_ant.strftime("%Y-%m-%d")
      @hora_ant = @hora_ant.strftime("%k:%M:%S.00000")

      @hora_act = Time.now
      @hora_act = @hora_act.strftime("%k:%M:%S.00000")

      @perAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_ant).where(:Fecha => @fecha).count
      @perAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_act).where(:Fecha => @fecha).count

      @tranAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).where("Hora <= ?", @hora_ant).where(:Fecha => @fecha).count
      @tranAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).where("Hora <= ?", @hora_act).where(:Fecha => @fecha).count

      if @perAct > @perAnt || @tranAct > @tranAnt
        @mensaje = true
      else
        @mensaje = false
      end

      render :partial => "par2"
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  def par3
    if current_user.habilitado == 0
      @hora_ant = Time.now - 30
      @fecha = @hora_ant.strftime("%Y-%m-%d")
      @hora_ant = @hora_ant.strftime("%k:%M:%S.00000")

      @hora_act = Time.now
      @hora_act = @hora_act.strftime("%k:%M:%S.00000")

      @tranAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).where("Hora <= ?", @hora_ant).where(:Fecha => @fecha).count
      @tranAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 1).where("Hora <= ?", @hora_act).where(:Fecha => @fecha).count

      if @tranAct > @tranAnt
        @mensajeTran = true
      else
        @mensajeTran = false
      end

      render :partial => "par3"
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  def par4
    if current_user.habilitado == 0
      @hora_ant = Time.now - 30
      @fecha = @hora_ant.strftime("%Y-%m-%d")
      @hora_ant = @hora_ant.strftime("%k:%M:%S.00000")

      @hora_act = Time.now
      @hora_act = @hora_act.strftime("%k:%M:%S.00000")

      @perAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_ant).where(:Fecha => @fecha).count
      @perAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_act).where(:Fecha => @fecha).count

      if @perAct > @perAnt
        @mensajePer = true
      else
        @mensajePer = false
      end

      render :partial => "par4"
    else
      @Without_Permission = 100
      redirect_to home_index_path, :alert => t('all.not_enabled')
    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  # def mensajeAlerta
  #
  #   @hora_ant = Time.now - 30
  #   @hora_ant = @hora_ant.strftime("%k:%M:%S.00000")
  #
  #   @hora_act = Time.now
  #   @hora_act = @hora_act.strftime("%k:%M:%S.00000")
  #

  #   @perAnt = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora <= ?", @hora_ant).count
  #   @perAct = Tbitacora.where(:IdTipoReg => 3).where(:IdProducto => 2).where("Hora >= ?", @hora_act).count
  #
  #   if @transAct > @transAnt
  #
  #     @producto = 1
  #     @mensajeTran = true
  #
  #   else
  #
  #     @mensaje = false
  #
  #   end
  #
  #   if @perAct > @perAnt
  #
  #     @producto = 2
  #     @mensajeTran = true
  #
  #   else
  #
  #
  #
  #     @mensaje = false
  #
  #   end
  #
  #   render :partial => "mensajeAlerta"
  #
  # end

end
