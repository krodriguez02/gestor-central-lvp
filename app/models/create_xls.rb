class CreateXls

  # ################## Exportacion de Alertas ---> alerTran y alertTPer
  # ##### Parametros
  def setInfoAlertXls(id, user, idioma, observaciones, historial, transacciones, arrCabecera_excel)
    @id = id
    @user_id = user
    @idioma = idioma
    @observaciones = observaciones
    @historial = historial
    @transacciones = transacciones
    @cols_tt = arrCabecera_excel

    @field_ref = Referenciadealerta.select(:idCampo, :campo).all.group(:idCampo, :campo)
    @longitud2 = @cols_tt.length

    @alerta = Tbitacora.find(@id)
  end

  def alertXls
    begin
      ### Se guardan todos los filtros existentes
      hash_filtros = {}
      @filtros = TxFilters.all
      @filtros.each do |filtros|
        hash_filtros.store(filtros.Id, "#{filtros.Id} - #{filtros.Description}")
      end

      ### Se guardan los estados en un hash
      has_estados = {}
      estados = Testadosxalerta.all
      estados.each do |estaDos|
        has_estados.store(estaDos.IdEstado, estaDos.Descripcion)
      end

      ### Se guardadn todos los usuarios en un Hash
      hash_users = {}
      @all_users = User.all
      @all_users.each do |users|
        hash_users.store(users.id, "#{users.id} - #{users.name} #{users.last_name}")
      end

      puts '>------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Alerta' + @id)
      @fecha_comienzo = Time.now
      ## -------------------- COMIENZA LA CREACIÓN DEL EXCEL ------------------------##

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 10, 12, 16, 10, 18, 12, 10.2, 14.2, 11, 12, 24, 10, 12, 10, 10, 10]
      col_widths_tran = [10, 10, 12, 16, 10, 18, 12, 10.2, 14.2, 11, 12, 24, 10, 12, 10, 10, 10]
      col_widths_per = [10, 10, 13, 12, 13, 27.3, 16, 16.8, 11, 17, 13, 14, 10, 20, 18, 20, 10]
      col_widths_h3 = [10, 11, 15, 11, 16, 13, 21, 12, 14.2, 15, 12, 12, 24, 10, 11, 10, 10, 10]
      p = Axlsx::Package.new
      wb = p.workbook
      sleep(2)

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------- HOJA DETALLE DE ALERTA -------------------------**************************##
        wb.add_worksheet(name: "Alert Detail") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('D2:P5') #Título del reporte
          sheet.merge_cells('B2:C6') #logo
          sheet.merge_cells('D6:E6') #fecha creación título
          sheet.merge_cells('F6:P6') #fecha creación cont

          #------- Observaciones Filas combinadas
          sheet.merge_cells('B15:P15')

          #------- Observaciones Filas combinadas
          sheet.merge_cells('B21:P21') #observaciones header
          sheet.merge_cells('B23:D23') #fecha/hora header
          sheet.merge_cells('E23:I23') #observacion header
          sheet.merge_cells('J23:L23') #estado header
          sheet.merge_cells('M23:P23') #usuario header


          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          sheet.add_row [" ", " ", " ", "Numero_de_Tarjeta: " + @alerta.IdTran, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]


          ##----------------------- DETALLE -------------------------##

          ### No hay pedro dijo pablo
          if @alerta.IdProducto == 1
            #------- Detalle
            sheet.merge_cells('B9:P9') #detalle header
            sheet.merge_cells('B11:D11') #ticket header
            sheet.merge_cells('B12:D12') #ticket content
            sheet.merge_cells('E11:H11') #fecha/hora header
            sheet.merge_cells('E12:H12') #fecha/hora content
            sheet.merge_cells('I11:L11') #origen header
            sheet.merge_cells('I12:L12') #origen content
            sheet.merge_cells('M11:P11') #filtro header
            sheet.merge_cells('M12:P12') #filtro content

            @origen = "Transaccional"

            if @idioma == 'es'
              sheet.add_row [" ", "Detalle de Alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Fecha/Hora ", " ", " ", " ", "Origen ", " ", " ", " ", "Filtro ", " ", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            elsif @idioma == 'en'
              sheet.add_row [" ", "Alert Detail", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Date/Hour ", " ", " ", " ", "Origin ", " ", " ", " ", "Filter ", " ", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            end

            if @alerta.tx_filters.present?
              @filtro = @alerta.tx_filters.Id.to_s + " - " + @alerta.tx_filters.Description.to_s
            else
              @filtro = "N/A"
            end

            sheet.add_row [" ", @alerta.Id, " ", " ", @alerta.Fecha + " " + @alerta.Hora, " ", " ", " ", @origen, " ", " ", " ", @filtro, " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          else
            #------- Detalle
            sheet.merge_cells('B9:P9') #detalle header
            sheet.merge_cells('B11:D11') #ticket header
            sheet.merge_cells('B12:D12') #ticket content
            sheet.merge_cells('E11:G11') #fecha/hora header
            sheet.merge_cells('E12:G12') #fecha/hora content
            sheet.merge_cells('H11:J11') #origen header
            sheet.merge_cells('H12:J12') #origen content
            sheet.merge_cells('K11:M11') #grupo header
            sheet.merge_cells('K12:M12') #grupo content
            sheet.merge_cells('N11:P11') #perfil header
            sheet.merge_cells('N12:P12') #perfil content

            @origen = "Perfiles"

            if @idioma == 'es'
              sheet.add_row [" ", "Detalle de Alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Fecha/Hora ", " ", " ", "Origen", " ", " ", "Grupo", " ", " ", "Perfil", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            elsif @idioma == 'en'
              sheet.add_row [" ", "Alert Detail", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
              sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
              sheet.add_row [" ", "Ticket ", " ", " ", "Date/Hour ", " ", " ", "Origin", " ", " ", "Group", " ", " ", "Profile", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
            end

            if @alerta.tgrupos.present?
              @grupo = @alerta.IdGrupo.to_s + " - " + @alerta.tgrupos.Nombre.to_s
            else
              @grupo = "N/A"
            end

            if @alerta.tperfiles.present?
              @perfil = @alerta.IdPerfil.to_s + " - " + @alerta.tperfiles.Nombre.to_s
            else
              @perfil = "N/A"
            end

            sheet.add_row [" ", @alerta.Id, " ", " ", @alerta.Fecha + " " + @alerta.Hora, " ", " ", @origen, " ", " ", @grupo, " ", " ", @perfil, " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          end
          ### No hay peodro

          ##----------------------- REFERENCIA -------------------------##

          if @idioma == 'es'
            @titleRef = 'Detalle de Referencia'
          elsif @idioma == 'en'
            @titleRef = 'Reference Detail'
          end

          sheet.add_row [" ", "#{@titleRef}", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          # sheet.add_row [" ", "Ticket ", " ", " ", "Fecha/Hora ", " ", " ", "Origen", " ", " ", "Grupo", " ", " ", "Perfil", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]


          @camposref = @alerta.Referencia.split(">")

          referencia = Array.new
          arr_cabecera = [" "]
          arr_cabecera_style = [nil]
          arr_valor = [" "]
          arr_valor_style = [nil]
          arr_valor_type = [:string]

          arr_vacio = [""]
          arr_vacio_style = [nil]
          arr_vacio2 = [""]

          @camposref.each do |valor|
            ref = {encabezado: ' ', val: ' '}

            encabezado = valor.split("=").first.from(5)
            encabezado = encabezado.split.join(" ")
            arr_cabecera.push(encabezado)
            arr_cabecera_style.push(highlight_cell)
            arr_valor_style.push(content_cell)
            arr_valor_type.push(:string)
            arr_vacio.push("")
            arr_vacio2.push("")
            arr_vacio_style.push(bottom)

            if valor.split("=").length > 1
              ref_valor = valor.split("=").last.split("<").last
              ref_valor = ref_valor.split.join(" ")
              arr_valor.push(ref_valor)
            else
              ref_valor = "N/A"
              arr_valor.push(ref_valor)
            end

            ref[:encabezado] = encabezado
            ref[:val] = ref_valor

            referencia.push(ref)
          end

          arr_cabecera.push(" ")
          arr_valor.push(" ")

          # puts "Soy arr_cabecera #{arr_cabecera}"
          # puts "Soy arr_cabecera -1  #{arr_cabecera.length - 1}"

          @columna_fin_cab = arr_cabecera.length - 1
          @columna_fin_cab = @columna_fin_cab.to_s26.to_s.upcase

          ## Nombre de los campos para la cabecera
          sheet.add_row arr_cabecera, style: arr_cabecera_style

          # @estilos_columnas_header_cab = 'B17:' + @columna_fin_cab + '17'
          # sheet[@estilos_columnas_header_cab].each {|c| c.style = highlight_cell}

          #Iprimiendo valores para Referencia
          # @row = 18
          sheet.add_row arr_valor, style: arr_valor_style, types: arr_valor_type
          # @estilos_valor = 'B' + @row.to_s + ':' + @columna_fin_cab + @row.to_s
          # sheet[@estilos_valor].each {|c| c.style = content_cell}
          # sheet[@estilos_valor].each {|c| c.style = content_cell}
          # sheet[@estilos_valor].each {|c| c.type = :string}


          # sheet.add_row [" ", "Numero_de_Tarjeta", "ID_Comer", "Term_ID_Term", "Hora_Trans", "TERM_NAME_LOC", "Nombre_de_terminal", "Ciudad_terminal", "Codigo_Respuesta", "Monto1", "Entry_mode", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          # sheet.add_row [" ", "1000000501158804", "394908091", "F0560055", "17151090", "TERMINAL DE APOYO", "005101530", "FF LEON", "000", "55000", "020", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
          sheet.add_row arr_vacio, style:arr_vacio_style #, style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          sheet.add_row arr_vacio2

          ##----------------------- Observaciones -------------------------##

          if @idioma == 'es'
            sheet.add_row [" ", "Observaciones", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Fecha/Hora", " ", " ", "Observaciones", " ", " ", " ", " ", "Estado", " ", " ", "Usuario de Atención", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Observations", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Date/Hour", " ", " ", "Observations", " ", " ", " ", " ", "State", " ", " ", "User of Attention", " ", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end


          # @observaciones = Tbitaobse.where(:IdAlerta => @alerta.Id).order("Id Desc")

          puts "Comienza imprimiendo Observaciones: #{Time.now.strftime("%H:%M:%S")}"
          @renglon = 23
          if @observaciones.present? && !@observaciones.nil?

            @observaciones.each do |ob|
              @renglon += 1
              @comb_fecha = 'B' + @renglon.to_s + ':D' + @renglon.to_s
              @comb_observ = 'E' + @renglon.to_s + ':I' + @renglon.to_s
              @comb_estado = 'J' + @renglon.to_s + ':L' + @renglon.to_s
              @comb_usuario = 'M' + @renglon.to_s + ':P' + @renglon.to_s
              sheet.merge_cells(@comb_fecha) #fecha content
              sheet.merge_cells(@comb_observ) #observaciones content
              sheet.merge_cells(@comb_estado) #estado content
              sheet.merge_cells(@comb_usuario) #usuario content

              if ob.user.present?
                @usuario = ob.IdUsuarioAtencion.to_s + " - " + ob.user.name + " " + ob.user.last_name
              else
                @usuario = "N/A"
              end
              sheet.add_row [" ", ob.Fecha + " " + ob.Hora, " ", " ", ob.Observaciones, " ", " ", " ", " ", ob.IdEstado.to_s + " - " + ob.testadosxalerta.Descripcion, " ", " ", @usuario, " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end
          else
            sheet.merge_cells('B24:P24') #observaciones nulas

            if @idioma == 'es'
              sheet.add_row [" ", "No hay observaciones para esta alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            elsif @idioma == 'en'
              sheet.add_row [" ", "There are no observations for this alert", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end

          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]

          ##----------------------- CERRANDO EXCEL -------------------------##

          if @alerta.IdProducto == 1
            sheet.column_widths *col_widths_tran
          else
            sheet.column_widths *col_widths_per
          end
          puts "Termina imprimiendo Observaciones: #{Time.now.strftime("%H:%M:%S")}"
          puts('Terminando hoja 1...')
          sleep(2)
        end

        wb.add_worksheet(name: "Alert History") do |sheet|
          #FILAS COMBINADAS
          sheet.merge_cells('D2:P5') #Título del reporte
          sheet.merge_cells('B2:C6') #logo
          sheet.merge_cells('D6:E6') #fecha creación título
          sheet.merge_cells('F6:P6') #fecha creación cont

          #ENCABEZADOS
          sheet.merge_cells('B9:P9') #historic header
          sheet.merge_cells('B11:C11') #ticket header
          sheet.merge_cells('D11:F11') #fecha-hora header
          sheet.merge_cells('G11:H11') #origen header
          sheet.merge_cells('I11:K11') #filtro header
          sheet.merge_cells('L11:N11') #estado header
          sheet.merge_cells('O11:P11') #usuario header

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          sheet.add_row [" ", " ", " ", "Numero_de_tarjeta: " + @alerta.IdTran, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- Historial de Alertas -------------------------##

          if @idioma == 'es'
            sheet.add_row [" ", "Historial de Alertas", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Ticket ", " ", "Fecha/Hora", " ", " ", "Origen", " ", "Filtro", " ", " ", "Estado", " ", " ", "Analista", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Alert History", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell, section__cell]
            sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
            sheet.add_row [" ", "Ticket ", " ", "Date/Hour", " ", " ", "Origin", " ", "Filter", " ", " ", "State", " ", " ", "Analyst", " ", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end

          # @historial = Tbitacora.where(:IdTran => @alerta.IdTran).where.not(:Id => @alerta.Id).where(:IdProducto => 1).order("Fecha Desc").order("Hora Desc")

          @renglon_h = 11
          if @historial.present? && !@historial.nil?

            puts "Comienza imprimiendo Hitorial de Alertas: #{Time.now.strftime("%H:%M:%S")}"
            @historial.each do |his|
              @renglon_h += 1
              @comb_ticket = 'B' + @renglon_h.to_s + ':C' + @renglon_h.to_s
              @comb_fecha = 'D' + @renglon_h.to_s + ':F' + @renglon_h.to_s
              @comb_origen = 'G' + @renglon_h.to_s + ':H' + @renglon_h.to_s
              @comb_filtro = 'I' + @renglon_h.to_s + ':K' + @renglon_h.to_s
              @comb_state = 'L' + @renglon_h.to_s + ':N' + @renglon_h.to_s
              @comb_user = 'O' + @renglon_h.to_s + ':P' + @renglon_h.to_s
              sheet.merge_cells(@comb_ticket) #ticket content
              sheet.merge_cells(@comb_fecha) #fecha content
              sheet.merge_cells(@comb_origen) #origen content
              sheet.merge_cells(@comb_filtro) #filtro content
              sheet.merge_cells(@comb_state) #estado content
              sheet.merge_cells(@comb_user) #usuario content

              if his.IdProducto == 1
                @origen = "Transaccional"
              else
                @origen = "Perfiles"
              end

              # Cambios Jose Luis
              if hash_filtros[his.IdFiltro].present?
                @filtro = hash_filtros[his.IdFiltro]
              else
                @filtro = "N/A"
              end

              # if his.tx_filters.present?
              #   @filtro = his.tx_filters.Id.to_s + " - " + his.tx_filters.Description
              # else
              #   @filtro = "N/A"
              # end

              # if his.testadosxalerta.present?
              #   @estado = his.IdEstado.to_s + " - " + his.testadosxalerta.Descripcion
              # else
              #   @estado = "N/A"
              # end
              #  puts his.IdUsuario
              #  @idUserT = "#{his.IdUsuario}"
              #  puts "Soy total hash_users: #{hash_users}"
              #  puts "Id: #{his.Id}  User: #{hash_users[his.IdUsuario]}  Esta el usuario #{hash_users[his.IdUsuario].present?}"
              if his.IdUsuario != "N/A"
                @idUser = his.IdUsuario.to_i
              else
                @idUser = his.IdUsuario
              end

              # puts "usuarioESSS #{hash_users[@idUser]}"

              if hash_users[@idUser].present?
                @usuario = hash_users[@idUser]
              else
                @usuario = "N/A"
              end

              puts "Tickec: #{his.Id} Fecha/Hora: #{his.Fecha} Origen: #{@origen} Filtro: #{@filtro} Estado: #{his.IdEstado} - #{has_estados[his.IdEstado]} Analasta: #{hash_users[his.IdUsuario]}"
              sheet.add_row [" ", his.Id, " ", his.Fecha + " " + his.Hora, " ", " ", @origen, " ", @filtro, " ", " ", "#{his.IdEstado} - #{has_estados[his.IdEstado]}", " ", " ", @usuario, " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end

            puts "Termino imprimiendo Hitorial de Alertas: #{Time.now.strftime("%H:%M:%S")}"
          else
            sheet.merge_cells('B12:P12') #historial nulo

            if @idioma == 'es'
              sheet.add_row [" ", "No existe información histórica para esta alerta", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            elsif @idioma == 'en'
              sheet.add_row [" ", "There is no historic content for this alert", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
          ##----------------------- CERRANDO EXCEL -------------------------##
          sheet.column_widths *col_widths
          puts('Terminando hoja 2...')
          sleep(2)
        end

        wb.add_worksheet(name: "Transactional History") do |sheet|
          @count_field = @longitud2
          puts green("Campos de referencia: #{@count_field}")
          @columna_fin = @count_field + 2 # Mas margen y la columna ticket
          @columna_fin = @columna_fin.to_s26.to_s.upcase

          puts @columna_fin

          #FILAS COMBINADAS
          @titulo = 'C2:' + @columna_fin + '5'
          @fecha_creacion_cont = 'E6:' + @columna_fin + '6'
          sheet.merge_cells(@titulo) #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells(@fecha_creacion_cont) #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          arr_row_header = Array.new
          arr_row_header.push(" ")

          arr_empty = Array.new
          arr_empty.push (" ")
          arr_empty.push (" ")

          col_widths_ref = Array.new
          col_widths_ref.push(10)
          col_widths_ref.push(17.5)

          for i in 0..@count_field
            arr_row_header.push(" ")
            arr_empty.push(" ")
            col_widths_ref.push(25)
          end


          ##---------------------------------- HEADER, LOGO ------------------------------ ##

          sheet.add_row arr_row_header
          if @idioma == 'es'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Numero_de_Tarjeta: " + @alerta.IdTran)
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Fecha de exportación: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}

          elsif @idioma == 'en'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Numero_de_Tarjeta: " + @alerta.IdTran)
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Export Date: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- Historial de Alertas Titulo -------------------------##
          #FILAS COMBINADAS
          @titulo_hist = 'B9:' + @columna_fin + '9'
          sheet.merge_cells(@titulo_hist) #Título del reporte

          if @idioma == 'es'
            sheet.add_row [" ", "Historial Transaccional"], style: [nil, section__cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Transactional History"], style: [nil, section__cell]
          end


          ## Agregando cabecera de Campos
          head_ref = [" ","Fecha/Hora"]
          head_ref_val = []
          head_ref_style = [nil]

          @cols_tt.each do |ref_l|
            # @campor = ref.campo.remove("'")
            @campor = ref_l.remove("'")
            head_ref.push(@campor)
            head_ref_val.push(@campor)
            head_ref_style.push(highlight_cell)
          end

          puts "Array Cabecera limpio: #{head_ref}"

          sheet.add_row head_ref, style: head_ref_style

          if @transacciones.present? && !@transacciones.nil?


            puts "Estas son las transac: #{@transacciones}"

            @transacciones.each do |his|

              puts "Soy Transaccion #{his['FECHA_HORA_KM'].to_s}"
              # tran = Array.new
              # tran_style = Array.new
              #tran_datatype = Array.new

              tran = [" "]
              tran_style = [nil]
              tran_datatype = [nil]

              tran.push(his['FECHA_HORA_KM'].to_s)
              tran_style.push(content_cell)
              tran_datatype.push(:string)

              head_ref_val.each do |ref|
                @campor = ref
                if his[@campor].present?
                  tran.push(his[@campor].to_s)
                else
                  tran.push("N/A")
                end
                tran_style.push(content_cell)
                tran_datatype.push(:string)
              end

              sheet.add_row tran, style: tran_style, :types => tran_datatype
              # sheet.add_row [" ", his[:fecha_hora_km], his[:origenkm], his[:fecha_hora], his[:tipo_operacion], his[:monto], his[:tarjeta], his[:entry_mode], his[:cap_code], his[:merchan_type], his[:member_id], his[:terminal_id], his[:comercio], his[:ciudad], his[:poblacion], his[:aut_num], his[:afiliacion], " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [:string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]
            end
          else
            @cont_hist_null = 'B12:' + @columna_fin + '12'
            sheet.merge_cells(@cont_hist_null) #Título del reporte

            if @idioma == 'es'
              sheet.add_row [" ", "No existe información transaccional para esta alerta"], style: [nil, content_cell], :types => [:string, :string]
            elsif @idioma == 'en'
              sheet.add_row [" ", "There is no transactional content for this alert"], style: [nil, content_cell], :types => [:string, :string]
            end
          end

          bottom_space = [" "]
          style_bottom_l = [nil]

          @field_ref.each do |ref|
            bottom_space.push(" ")
            style_bottom_l.push(bottom)
          end

          sheet.add_row bottom_space , style: style_bottom_l
          ##----------------------- CERRANDO EXCEL -------------------------##
          # sheet.column_widths * col_widths_ref
          puts('Terminando hoja 3...')
          sleep(2)
        end

      end

      ##-------------------- GUARDANDO EXCEL ----------------------##
      # p.serialize('public/generatedXLS/reporteOperativo/reporteOperativo.xlsx')
      @serialTime = Time.now.strftime('%H%M%S%d%m%Y')
      p.serialize("public/generatedXLS/alerts/Alert#{@id}-#{@serialTime}-#{@user_id}.xlsx")

      @file = "Alert#{@id}-#{@serialTime}-#{@user_id}.xlsx"

      puts "Archivo creado: #{@file}"

      puts('Archivo almacenado...')

      @fecha_termino = Time.now
      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      tipo = 'Reporte Operativo'
      usuario = User.find(@user_id)
      if @idioma == 'es'
        XlsMailer.send_xls_es(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      elsif @idioma == 'en'
        XlsMailer.send_xls(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
        XlsMailer.send_xls_es(@fecha_comienzo, usuario, @file, @fecha_termino).deliver
      end

      puts 'Listo, proceso terminado'
    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  ################## Exportacion de Alertas ---> alerTran y alertTPer

  ################## Consulta General ---> con_gen Este es el chido
  def setInfoGenXls(query, usuarios_all, usuario_log, idioma)

    @registros = @alertas_xls = Tbitacora.where(query)
    @user_log = usuario_log
    all_users = usuarios_all
    # tformatos_all = tformatos
    # tlays_all = tlays
    # tiporegalarma_all = tiporegalarma

    tformatos_all = Tformatos.all
    tlays_all = Tlay.all
    tiporegalarma_all = Ttiporegalarma.all

    @idioma = idioma

    ### Se guardadn todos los usuarios en un Hash
    hash_users = {}
    all_users.each do |users|
      hash_users.store(users.id, "#{users.name} #{users.last_name}")
    end

    ### Se guardan todos los tformatos en un Hash
    hash_tformatos = {}
    tformatos_all.each do |form|
      hash_tformatos.store(form.IdFormato, "#{form.Descripcion}")
    end

    # puts "#{hash_tformatos}"

    ### Se guardan todos los tlayen un Hash
    hash_tlay = {}
    tlays_all.each do |lay|
      hash_tlay.store(lay.IdLay, "#{lay.Descripcion}")
    end
    # puts "#{hash_tlay}"

    hash_tipoAlarma = {}
    tiporegalarma_all.each do |alarm|
      hash_tipoAlarma.store(alarm.IdTipoReg, "#{alarm.Descripcion}")
    end
    # puts "#{hash_tipoAlarma}"

    hash_tipoProducto = { 1 => "Transaccional", 2 => "Perfiles"}
    hash_typeProduct = { 1 => "Transactional", 2 => "Profiles"}


    begin
      puts '>------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Con-Gen')
      puts green("Cantidad de registros: #{@registros.count}")
      @fecha_comienzo = Time.now


      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 15, 15, 17, 17, 30, 15, 15, 20, 70.5, 10] #Se contruye el tamaño de las celdas.
      p = Axlsx::Package.new #Se crea un nuevo libro en excel
      p.use_autowidth = true
      wb = p.workbook


      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------  HOJA CONSULTA GENERAL -------------------------**************************##
        wb.add_worksheet(name: "General") do |sheet|
          #FILAS COMBINADAS
          sheet.merge_cells('C2:J5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:J6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 120
            image.height = 93
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          if @idioma == 'es'
            sheet.add_row [" ", " ", "Resultados de Consulta General", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Result from General Search", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, left_thick]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- ALERTAS -------------------------#
          if @idioma == 'es'
            sheet.add_row [" ", "Ticket", "Producto", "Layout", "Formato", "Fecha/Hora", "IP", "Usuario", "Tipo de Resgistro", "Mensaje", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Ticket", "Product", "Layout", "Format", "Date/Hour", "IP", "User", "Register_Type", "Message", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end

          @registros.each do |alertas_excel|

            puts "Valores de la alerta ::::::::: #{alertas_excel.Id}"

            if hash_users[alertas_excel.IdUsuario.to_i].present?
              @usuario = "#{hash_users[alertas_excel.IdUsuario.to_i]}"
            else
              @usuario = 'N/A'
            end

            if @idioma == 'es'
              # hash_tipoProducto = { 1 => "Transaccional", 2 => "Perfiles"}
              if hash_tipoProducto[alertas_excel.IdProducto].present?
                @producto = hash_tipoProducto[alertas_excel.IdProducto]
              end
            elsif @idioma == 'en'
              if hash_typeProduct[alertas_excel.IdProducto].present?
                @producto = hash_tipoProducto[alertas_excel.IdProducto]
              end
            end

            # sheet.add_row [" ", "#{alertas_excel.Id}", "#{alertas_excel.IdProducto}", al.tlay.Descripcion, al.tformatos.Descripcion, al.Fecha.to_s + ' ' + al.Hora.to_s, al.IP.to_s, al.IdUsuario.to_s, al.ttiporegalarma.Descripcion.to_s, al.Mensaje.to_s,  " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]
            sheet.add_row [" ", "#{alertas_excel.Id}", "#{@producto}", "#{hash_tlay[alertas_excel.IdLay]}", "#{hash_tformatos[alertas_excel.IdFormato]}", alertas_excel.Fecha.to_s + ' ' + alertas_excel.Hora.to_s, alertas_excel.IP.to_s, "#{@usuario}", "#{hash_tipoAlarma[alertas_excel.IdTipoReg]}", alertas_excel.Mensaje.to_s, " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]

          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]

          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths

          puts('Terminando hoja 1...')

        end
      end
      ##**************************----------------------- TERMINAN LIBRO DE EXCEL -------------------------**************************##

      sleep(2)
      ##-------------------- GUARDANDO EXCEL ----------------------##
      @serialTime = Time.now.strftime('%H%M%S%d%m%Y')
      @idUser = @user_log.id
      p.serialize("public/generatedXLS/con_gen/ResultExportfromGeneralSearch-#{@serialTime}-#{@idUser}.xlsx")
      @name_file = "ResultExportfromGeneralSearch-#{@serialTime}-#{@idUser}.xlsx"
      @path_file = "public/generatedXLS/con_gen/#{@name_file}"

      puts "Archivo creado:   #{@name_file}"

      puts('Archivo almacenado...')

      @fecha_termino = Time.now

      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #

      if @idioma == 'es'
        XlsConGenMailer.send_con_gen_xls_es(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      elsif @idioma == 'en'
        XlsConGenMailer.send_con_gen_xls_en(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
      end

      puts 'Listo, proceso terminado'

    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  ################## Consulta General ---> con_gen Este es el chido

  #### ESTE ES EL CHIDO alertc y niv_alertamiento
  def setInfoAlertLevelXls(filtro_generador, usuario_log, usuarios_all, query, idioma)

    puts "Entre Metodo setInfoAlertLevelXls"

    @filtro_vista = filtro_generador



    ## Se define la consulta dependiendo de que controlador viene la peticion
    if @filtro_vista == "states"
      # Query para Niveles de Alertamiento
      puts query
      @alertas = Tbitacora.where(query + " and IdTipoReg = 3")
      # @alertas = Tbitacora.where("IdEstado in (2,3) and IdTipoReg = 3")
      @cotalert = Tbitacora.group('IdEstado').where(query + " and IdTipoReg = 3").count

      # @cotalert=Tbitacora.group('IdEstado').where("IdEstado in (2,3) and IdTipoReg = 3").count
    elsif @filtro_vista == "users"
      @alertas = Tbitacora.where(query + " and IdTipoReg = 3")
      @cotalert = Tbitacora.group('IdUsuario').where(query + " and IdTipoReg = 3").count
    elsif @filtro_vista == "general transaccional"
      # Query para Consulta de aletas
      @alertas = Tbitacora.where(query + " and IdProducto = 1 and IdTipoReg = 3")
    elsif @filtro_vista == "general perfiles"
      # Query para Consulta de aletas
      @alertas = Tbitacora.where(query + " and IdProducto = 2 and IdTipoReg = 3")
    end

    @filtro = filtro_generador
    @user_log = usuario_log
    @all_users = usuarios_all
    @field_ref = Referenciadealerta.select(:idCampo, :campo).all.group(:idCampo, :campo)
    @longitud2 = @field_ref.length #ActiveRecord::Base::connection.exec_query("SELECT COUNT(*) as contador FROM (SELECT [referenciadealerta].[idCampo], [referenciadealerta].[campo] FROM [referenciadealerta] GROUP BY [referenciadealerta].[idCampo], [referenciadealerta].[campo]) as A")
    @idioma = idioma

    puts "Termine consultas del metodo"
  end

  def alertLevelXLS

    longitud = @longitud2
    count = 0

    ### Se guardadn los estados en un hash
    has_estados = {}
    estados = Testadosxalerta.all
    estados.each do |estaDos|
      has_estados.store(estaDos.IdEstado, estaDos.Descripcion)
      puts has_estados
    end
    # puts "#{has_estados}"
    # puts "Valor!: #{has_estados[1]}"

    ### Se guardadn todos los usuarios en un Hash
    hash_users = {}
    @all_users.each do |users|
      hash_users.store(users.id, users.name + users.last_name)
      puts hash_users
    end

    ## Se guardan todos las Referencias
    hash_tabla_ref = {}
    hash_ref_alertas = {}
    array_alertas_todo = []

    @field_ref.each do |ref_field|
      hash_tabla_ref.store(ref_field.idCampo, ref_field.campo)
    end

    ######### Aqui va el codigo
    #puts "#{@alertas}"

    #puts "Start: #{Time.now.strftime('%H%M%S')}"

    begin

      puts '>------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<'
      puts green('Iniciando Hilo: Creación de XLS-Niveles de Alertamiento')
      puts green("Cantidad de registros: #{@alertas.count}")
      @fecha_comienzo = Time.now
      ## -------------------- COMIENZA LA CREACIÓN DEL EXCEL ------------------------##

      ##------------------------------ ANCHO DE LAS FILAS ------------------------------- ##
      col_widths = [10, 17.5, 21, 15, 15, 118, 15, 20, 10]
      p = Axlsx::Package.new
      p.use_autowidth = true
      wb = p.workbook

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'}, :format_code => "0")
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------- HOJA ALERTAS -------------------------**************************##
        wb.add_worksheet(name: "Alerts") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:H5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:H6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          if @idioma == 'es'
            # VIENE DE ALERTC CONTROLLER
            if @filtro == 'general transaccional' || @filtro == 'general perfiles'
              if @filtro == 'general transaccional'
                sheet.add_row [" ", " ", "Resultado de Alertas de KM Transaccional", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              elsif @filtro == 'general perfiles'
                sheet.add_row [" ", " ", "Resultado de Alertas de KM Perfiles", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              end
            else # VIENE DE NIV ALERTAMIENTO CONTROLLER
              if @filtro == 'states'
                @filtro = 'estados'
              elsif @filtro == 'users'
                @filtro = 'usuarios'
              end

              sheet.add_row [" ", " ", "Resultado de Alertas Filtro" + @filtro.titleize, " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
            end
          elsif @idioma == 'en'
            # VIENE DE ALERTC CONTROLLER
            if @filtro == 'general transaccional' || @filtro == 'general perfiles'
              if @filtro == 'general transaccional'
                sheet.add_row [" ", " ", "Alerts Result KM Transaccional", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              elsif @filtro == 'general perfiles'
                sheet.add_row [" ", " ", "Alerts Result KM Perfiles", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
              end
            else # VIENE DE NIV ALERTAMIENTO CONTROLLER
              sheet.add_row [" ", " ", "Alerts Result " + @filtro.titleize + " Filter", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, left_thick]
            end

          end
          ##---------------------------------- TERMINA HEADER, LOGO ------------------------------ ##

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, left_thick]


          if @idioma == 'es'
            sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, left_thick]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- ALERTAS -------------------------#
          if @idioma == 'es'
            sheet.add_row [" ", "Ticket", "Fecha/Hora", "Origen", "Cuenta_CI", "Mensaje", "Estado", "Analista", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [" ", "Ticket", "Date/Hour", "Origin", "CI_Account", "Message", "State", "Analyst", " "], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end

          hash_tipoAl = {1 => "Transaccional", 2 => "Perfiles"}
          @alertas.each do |alertas_excel|

            count += 1

            if hash_users[alertas_excel.IdUsuario.to_i].present?
              @usuario = "#{hash_users[alertas_excel.IdUsuario.to_i]}"
            else
              @usuario = 'N/A'
            end

            sheet.add_row [" ", "#{alertas_excel.Id}", "#{alertas_excel.Fecha} #{alertas_excel.Hora}", "#{hash_tipoAl[alertas_excel.IdProducto]}", "#{alertas_excel.IdTran}", "#{alertas_excel.Mensaje}", "#{alertas_excel.IdEstado} - #{has_estados[alertas_excel.IdEstado]}", @usuario, " "], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [nil, :string, :string, :string, :string, :string, :string, :string]

            # puts "TotalAlertas: #{count}"
          end

          puts "#{Time.now.strftime("%H:%M:%S")} Total Alertas Hoja1: #{count}"

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths

          puts('Terminando hoja 1...')
          sleep(2)
        end
        ##**************************----------------------- TERMINA HOJA ALERTAS -------------------------**************************##


        #puts "Inicio: #{Time.now.min}"

        @alertas.each do |ref_alert|
          hash_val_referencia = {}
          array_val_ref = []

          # puts "Valores de referencia: #{ref_alert.Referencia}"

          @ref_alert = ref_alert.Referencia.split('>')

          begin
            @ref_alert.each do |rf_al|
              if (!rf_al.nil?) && (rf_al.include? "-")
                campo = rf_al.split("-").first.from(1).to_i
                valor = rf_al.split("=").second
                hash_val_referencia.store(campo, valor)
              end
            end
          rescue => e
            puts "Valores de referencia: #{ref_alert.Referencia}"
            puts red('Archivo: log/error_get_referencia.log')
            logger = Logger.new("log/error_get_referencia.log")
            logger.error('----------------------------------------------------------')
            logger.error(e)
            logger.error('----------------------------------------------------------')
            logger.error(ref_alert.Referencia)
          end
          array_val_ref.push(" ")
          array_val_ref.push(ref_alert.Id)
          hash_tabla_ref.each do |key, valor|

            if hash_val_referencia[key].present?
              campo = hash_val_referencia[key] #hash_tabla_ref[campo]
            else
              campo = 'N/A'
            end
            array_val_ref.push(campo)
          end
          array_alertas_todo.push(array_val_ref)
        end

        # puts "#{array_alertas_todo}"
        # array_alertas_todo.each do |arr_val|
        #   # puts "#{arr_val}"
        # end

        # puts "End: #{Time.now.strftime('%H%M%S')}"

        ##**************************----------------------- HOJA REFERENCIAS -------------------------**************************##
        wb.add_worksheet(name: "References") do |sheet|
          @count_field = longitud
          puts green("Campos de referencia: #{@count_field}")
          @columna_fin = @count_field + 2 # Mas margen y la columna ticket
          @columna_fin = @columna_fin.to_s26.to_s.upcase

          # puts @columna_fin

          #FILAS COMBINADAS
          @titulo = 'C2:' + @columna_fin + '5'
          @fecha_creacion_cont = 'E6:' + @columna_fin + '6'
          sheet.merge_cells(@titulo) #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells(@fecha_creacion_cont) #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 140
            image.height = 101
            #image.start_at 2, 2
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end

          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          arr_row_header = Array.new
          arr_row_header.push(" ")

          arr_empty = Array.new
          arr_empty.push (" ")
          arr_empty.push (" ")

          col_widths_ref = Array.new
          col_widths_ref.push(10)
          col_widths_ref.push(17.5)

          for i in 0..@count_field
            arr_row_header.push(" ")
            arr_empty.push(" ")
            col_widths_ref.push(25)
          end


          sheet.add_row arr_row_header
          if @idioma == 'es'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Referencias de Alertas")
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Fecha de exportación: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}

          elsif @idioma == 'en'
            arr_titulo = arr_row_header
            arr_titulo.insert(2, "Alerts References")
            sheet.add_row arr_titulo, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            sheet.add_row arr_row_header, style: [nil, nil, center]
            @estilos = 'D2:' + @columna_fin + '5'
            sheet[@estilos].each {|c| c.style = border_thick}

            #Fecha
            arr_fecha = arr_row_header
            arr_fecha.insert(2, "Export Date: ")
            arr_fecha.insert(4, Time.now.to_date.strftime("%d/%m/%Y"))
            sheet.add_row arr_titulo, style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold]
            @estilos_fecha = 'F6:' + @columna_fin + '6'
            sheet[@estilos_fecha].each {|c| c.style = border_thick}
          end

          sheet.add_row [" ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" "]

          ##----------------------- COLUMNAS DE LA REFERENCIA -------------------------#
          arr_titulos_ref = Array.new
          arr_titulos_ref.push(" ")
          arr_titulos_ref.push("Ticket")

          ##----------------------- HEADER REFERENCIA -------------------------#
          arr_style = [nil, content_cell]
          arr_type_d = [nil, :string]
          @field_ref.each do |tit_ref|
            arr_titulos_ref.push(tit_ref.campo.strip)
            arr_style.push(content_cell)
            arr_type_d.push(:string)
          end

          sheet.add_row arr_titulos_ref, style: [nil]
          @estilos_columnas_header = 'B9:' + @columna_fin + '9'
          sheet[@estilos_columnas_header].each {|c| c.style = highlight_cell}


          @row = 9
          total_arreglo = 0
          array_alertas_todo.each do |arr_val|
            @row += 1
            #puts "Imprimir arreglo de alerta :::::::::: #{arr_val}"
            # sheet.add_row arr_val #, style: [nil]
            sheet.add_row arr_val, style: arr_style, :types => arr_type_d #, style: [nil]
            # @estilos_valor = 'B'+@row.to_s+':' + @columna_fin + @row.to_s
            # sheet[@estilos_valor].each {|c| c.style = content_cell}
            # sheet[@estilos_valor].each {|c| c.type = :string}
            total_arreglo += 1

            # puts "TotalRef: #{total_arreglo}"
          end
          puts "TotalRef: #{total_arreglo}"

          sheet.add_row arr_empty, style: [nil, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          @row = @row += 1
          @estilos_final = 'B' + @row.to_s + ':' + @columna_fin + @row.to_s
          sheet[@estilos_final].each {|c| c.style = bottom}
          ##----------------------- CERRANDO HOJA 2 -------------------------##
          sheet.column_widths *col_widths_ref

          # puts "#{Time.now.strftime("%H:%M:%S")} Total Referecnias Hoja2 #{total_arreglo}"
          puts('Terminando hoja 2...')
          sleep(2)

        end
        ##**************************----------------------- TERMINA HOJA REFERENCIAS -------------------------**************************##
        wb.add_worksheet(name: "Grafica") do |sheet|
          @dass=3;
          @totald=0;
          sheet.add_row ['']
          sheet.add_row ['','Tipo','Count'], style: [nil,highlight_cell,highlight_cell]


          @cotalert.each do |key, value|
            puts "holitas"
            puts key
            puts value
            @dass=@dass+1;
            if @filtro_vista == "states"
          label_dos = has_estados[key]
          sheet.add_row ['' ,label_dos,value]
            elsif @filtro_vista == "users"
              label_un = hash_users[key]
              puts "label_un ="
              puts label_un
              sheet.add_row ['' ,hash_users[key.to_i],value]
              end

            @totald=@totald+value
          end
          sheet.add_row ['' ,'total',@totald]
          @dass2=@dass+7

          sheet.add_chart(Axlsx::Bar3DChart, :bar_dir => :col) do |chart|
            chart.start_at 4,1
            chart.end_at @dass2,40
            chart.add_series :data => sheet["C3:C"<<@dass], :labels => sheet["B3:B"<<@dass], :title => "Alertas"
            chart.valAxis.gridlines = false
            chart.catAxis.gridlines = false
          end


        end


      end
      ##**************************----------------------- TERMINAN LIBRO DE EXCEL -------------------------**************************##


      ##-------------------- GUARDANDO EXCEL ----------------------##
      @serialTime = Time.now.strftime('%H%M%S%d%m%Y')
      @idUser = @user_log.id

      puts "#{@filtro}"

      if @filtro_vista == "states"
        p.serialize("public/generatedXLS/niv_alert/ResultsStatesFilterGenerator-#{@serialTime}-#{@idUser}.xlsx")
        #@file = "ResultsStatesFilterGenerator-#{Time.now.strftime('%H%M%S')}.xlsx"
        @name_file = "ResultsStatesFilterGenerator-#{@serialTime}-#{@idUser}.xlsx"
        @path_file = "public/generatedXLS/niv_alert/#{@name_file}"
      elsif @filtro_vista == "users"
        p.serialize("public/generatedXLS/niv_alert/ResultsUsersFilterGenerator-#{@serialTime}-#{@idUser}.xlsx")
        #@file = "ResultsUsersFilterGenerator-#{Time.now.strftime('%H%M%S')}.xlsx"
        @name_file = "ResultsUsersFilterGenerator-#{@serialTime}-#{@idUser}.xlsx"
        @path_file = "public/generatedXLS/niv_alert/#{@name_file}"

      elsif @filtro_vista == "general transaccional"
        p.serialize("public/generatedXLS/niv_alert/ResultsKMTxConsult-#{@serialTime}-#{@idUser}.xlsx")
        #@file = "ResultsKMTxConsult-#{Time.now.strftime('%H%M%S')}.xlsx"
        @name_file = "ResultsKMTxConsult-#{@serialTime}-#{@idUser}.xlsx"
        @path_file = "public/generatedXLS/niv_alert/#{@name_file}"
      elsif @filtro_vista == "general perfiles"
        p.serialize("public/generatedXLS/niv_alert/ResultsKMPerConsult-#{@serialTime}-#{@idUser}.xlsx")
        #@file = "ResultsKMPerConsult-#{Time.now.strftime('%H%M%S')}.xlsx"
        @name_file = "ResultsKMPerConsult-#{@serialTime}-#{@idUser}.xlsx"
        @path_file = "public/generatedXLS/niv_alert/#{@name_file}"
      end
      sleep(2)
      puts "Archivo creado: #{@name_file}"

      puts('Archivo almacenado...')
      sleep(2)

      @fecha_termino = Time.now
      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #


      sleep(2)
      if @idioma == 'es'
        XlsMailer.send_level_xls_es(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      elsif @idioma == 'en'
        XlsMailer.send_level_xls(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
      end

      puts 'Listo, proceso terminado'
    rescue => e
      puts red('Archivo: log/error_xls.log')
      logger = Logger.new("log/error_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)

      XlsMailer.error_send_xls(@fecha_comienzo, @user_log, @name_file, @fecha_termino).deliver
    end
  end

  #### ESTE ES EL CHIDO alertc y niv_alertamiento


  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end


end