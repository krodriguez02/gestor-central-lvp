class CreateFiltersnos < ActiveRecord::Migration[5.0]
  def change
    create_table :filtersnos do |t|
      t.integer :IdFiltro
      t.integer :IdUsuario
      t.string :Fecha

      t.timestamps
    end
  end
end
