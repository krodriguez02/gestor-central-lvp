namespace :reference do
  desc 'Tarea que se encarga de revisar la tabla Tbitacora y crear una nueva tabla para almacenar las referencias'

  task insert: :environment do

    begin
      puts green('------------------------- Comienza importación de referencias -------------------------')
      puts green(Time.now.to_s)

      base_utilizada = ActiveRecord::Base.connection.adapter_name.downcase

      puts magenta('Base de datos utilizada: ' + base_utilizada.to_s)

      base_utilizada = base_utilizada.to_sym

      case base_utilizada
        when :mysql
          sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
          sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
        when :mysql2
          sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
          sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
        when :sqlite
          sql_exist = 'SELECT * FROM %{reference_table} WHERE idAlerta = %{idAlerta} LIMIT 1'
          sql_insert = 'INSERT INTO %{reference_table} (idAlerta, idCampo, idProducto, campo, valor, created_at, updated_at) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
        when :sqlserver
          sql_exist = 'SELECT TOP(1) * FROM %{reference_table} WHERE idAlerta = %{idAlerta}'
          sql_insert = 'INSERT INTO [%{reference_table}] ([idAlerta], [idCampo], [idProducto], [campo], [valor], [created_at], [updated_at]) VALUES (%{idAlerta}, %{idCampo}, %{idProducto}, %{campo}, %{valor}, %{created_at}, %{updated_at})'
      end

      puts cyan('Buscando alertas')
      @alerts = Tbitacora.where(:IdTipoReg => 3)

      @cant_alertas = @alerts.count()
      @count = 0
      @alerts.each do |al|
        @count += 1
        puts('Alerta ' + @count.to_s + ' de ' + @cant_alertas.to_s)

        @idAlerta = al.Id
        @idProducto = al.IdProducto
        # puts (@idAlerta.to_s)

        reference_table = 'referenciadealerta'

        if (ActiveRecord::Base.connection.table_exists? reference_table) # Existe la tabla de referencias
          sql_exist_q = sql_exist % {reference_table: reference_table, idAlerta: @idAlerta}
          # puts (sql_exist_q)
          Timeout::timeout(0) {@alerta_existente = ActiveRecord::Base::connection.exec_query(sql_exist_q)}

          if !@alerta_existente.nil? # No existe la referencia de la alerta
            @camposref = al.Referencia.split(">")

            @camposref.each do |ref|
              @encabezado = ref.split("=").first
              @idCampo = @encabezado[1..3]
              @campo = "'#{@encabezado.from(5)}'"
              @valor = "'#{ref.split("=").second}'"
              @createDate = "'#{Time.now.to_datetime.strftime("%F %T")}'"

              #Insertar registro
              sql_insert_q = sql_insert % {reference_table: reference_table, idAlerta: @idAlerta, idCampo: @idCampo, idProducto: @idProducto, campo: "#{@campo}", valor: "#{@valor}", created_at: "#{@createDate}", updated_at: "#{@createDate}"}
              # puts(sql_insert_q)
              Timeout::timeout(0) {@inserted = ActiveRecord::Base::connection.exec_query(sql_insert_q)}

              if !@inserted.nil?
                puts red('Error: Referencia no insertada, ticket ' + @idAlerta.to_s + ', idCampo ' + @idCampo.to_s + ', valor ' + @campo.to_s)
              end
            end
          else # Si existe
            puts magenta('Ya existe la referencia para la alerta con ticket ' + @idAlerta.to_s)
          end
        else # No existe la tabla de referencias
          # Creación de tabla
          puts('No existe tabla de referencias')
          puts('Creando tabla: ' + reference_table.to_s)
          ActiveRecord::Migration.create_table(reference_table)
          ActiveRecord::Migration.add_column(reference_table, :idAlerta, :integer)
          ActiveRecord::Migration.add_column(reference_table, :idCampo, :integer)
          ActiveRecord::Migration.add_column(reference_table, :idProducto, :integer)
          ActiveRecord::Migration.add_column(reference_table, :campo, :string)
          ActiveRecord::Migration.add_column(reference_table, :valor, :string)
          ActiveRecord::Migration.add_column(reference_table, :created_at, :datetime)
          ActiveRecord::Migration.add_column(reference_table, :updated_at, :datetime)

          puts('Tabla creada.')
          @camposref = al.Referencia.split(">")

          @camposref.each do |ref|
            @idAlerta = al.Id
            @encabezado = ref.split("=").first
            @idCampo = @encabezado[1..3]
            @campo = "'#{@encabezado.from(5)}'"
            @valor = "'#{ref.split("=").second}'"
            @createDate = "'#{Time.now.to_datetime.strftime("%F %T")}'"

            #Insertar registro
            sql_insert_q = sql_insert % {reference_table: reference_table, idAlerta: @idAlerta, idCampo: @idCampo, idProducto: @idProducto, campo: "#{@campo}", valor: "#{@valor}", created_at: "#{@createDate}", updated_at: "#{@createDate}"}
            # puts(sql_insert_q)
            Timeout::timeout(0) {@inserted = ActiveRecord::Base::connection.exec_query(sql_insert_q)}

            if !@inserted.nil?
              puts red('Error: Referencia no insertada, ticket ' + @idAlerta.to_s + ', idCampo ' + @idCampo.to_s + ', valor ' + @campo.to_s)
            end
          end

        end
      end

      puts green('-------------------- Fin del proceso --------------------')
      puts green(Time.now.to_s)
    rescue => e
      puts red('ERROR ON REFERENCE INSERT TASK: ' + Time.now.to_s)
      puts red(e.to_s)

      logger = Logger.new('log/reference-err.log')
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  task prueba: :environment do
    c = CreateXls.new
    c.prueba
  end
  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def magenta(mytext)
    ; "\e[35m#{mytext}\e[0m"
  end
end
